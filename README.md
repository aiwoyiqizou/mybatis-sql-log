# mybatis-sql-log

#### 介绍
将mybatis的Preparing和Parameters的sql日志做一下拼接，生成数据库可执行SQL。类似于mybatis的插件:MyBatis Log Plugin


#### 使用说明

1.  将SqlLogImpl拷贝到自己的项目当中
2.  springboot的application属性文件中，设置了mybatis的输出类为：
`mybatis.configuration.log-impl=com.test.SqlLogImpl`
同样若是mybatis-plus则为： 
`mybatis-plus.configuration.log-impl=com.test.SqlLogImpl`

如果配置是xml：

```
   <settings>
      <setting name="logImpl" value="com.test.SqlLogImpl"/>
   </settings>
```

    
#### 效果
![输入图片说明](https://images.gitee.com/uploads/images/2020/0809/190658_acf64ca3_7503178.jpeg "sqllog.jpg")



